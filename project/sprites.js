console.log("sprites file is connected");

let row = 15;
let column = 15;
let empty = "E";
let square = 15;
let score =0;

let game = document.querySelector("gameboard");

function makeRect(x,y)
{
     let rectString = "<rect x='" +column*20+ "' y='" +row*20+ "' width='20' height='20' fill='red'/>"
}

function makeRect1(x,y)
{
    let rectString1 = "<rect x='" +column*20+ "' y='" +row*20+ "' width='20' height='20' fill='gray'/>"
}

function makeGrid(row,column)
{
    let grid= [];
    for(let m=0; m < row; m++){
        grid.push([]);
        for(let n =0; n < column; n++){
            grid[m].push('E');
        }
    }
    return grid;
}

let Grid = makeGrid(row,column);
let drawGame = makeGrid(row,column);
 generateScore();
console.log(drawGame);

function createGame(create)
{
    let String= " " ;
   // console.log(create)
    for(i=0; i<row; i++){
        for(j=0; j<column; j++){
            if(create[i][j] == "E")
            {
                areaString = "<rect x='" +column*20+ "' y='" +row*20+ "' width='20' height='20' fill='red'/>"
            }
        }
    }
}

function makeRect(x,y, color)
{
    return "<rect x='" +x+ "' y='" +y+ "' width='20' height='20' fill='" +color+"'/>"
}


let rowRange = [0,0];
let columnRange = [0,1];
  for (let j=0; j < rowRange.length; j ++) {
    for(let i=0; i < columnRange.length; i++) {
       if(drawGame[rowRange[j]][columnRange[i]] == "$"){
        score = score+2; }
        drawGame[rowRange[j]][columnRange[i]] = "O";
    }
   
}

  
function define()
{
    if(rowRange[rowRange.length-1] + 1  < row && check()){
        
        for( let i=0; i< rowRange.length; i++){
            rowRange[i] += 1;
        }
        //check scores
        for (let i=0; i < rowRange.length; i ++){
            for (let j=0; j< columnRange.length; j++){
                if (drawGame[rowRange[i]][columnRange[j]] == "$"){
                    score = score+2;                
                }
            }
        }        
        //set the previous ones to E
        for (let i=0; i < rowRange.length; i ++){
            for (let j=0; j< columnRange.length; j++){
                drawGame[rowRange[i]-1][columnRange[j]] = "E";
            }
        }
        //set the current ones to O  
        for (let i=0; i < rowRange.length; i ++){
            for (let j=0; j< columnRange.length; j++){
                drawGame[rowRange[i]][columnRange[j]] = "O";
            }
        }
    }
    else  
    {

        if(Math.random() < 0.2)
        {
            rowRange = [0,0,1,1];
            columnRange = [0,1,0,1];
        }
        else {
            rowRange = [0];
            columnRange = [0];
        }
        
        remove();

        for (let i=0; i < rowRange.length; i ++){
            for (let j=0; j< columnRange.length; j++){
                drawGame[rowRange[i]][columnRange[j]] = "O";
            }
        }  
    }
    
    generate();
    let points=document.querySelector("#points");
     points.innerHTML = score;
 }

 //true means I can go through it 
 function check() { 
    for(let j=0; j<columnRange.length; j++)
    {
        if (drawGame[rowRange.length-1] + 1 [columnRange[j]] == "O"){
            return false;
        }
    }
    return true;
 }

function generate()
{
    let output = "";
    for(let p=0; p< row; p++ ){
        for (let q=0; q< column; q++) {
            if (drawGame[p][q] == "O")
            {
               // console.log(p+" "+q);
                output += makeRect(q*20, p*20, "red");
            } else if(drawGame[p][q]=="$")
              {
                  output +=makeRect(q*20, p*20, "blue");
              }

             else{
                 output +=makeRect(q*20, p*20, "gray");
             }
        }
    }
    document.querySelector("#game").innerHTML = output;
}

setInterval(define,500);

function movePlayer(event)
 {
    if (event.key == 'l') 
    {
        for(let j=0; j < rowRange.length; j++)
        {
            for(let i=0; i<columnRange.length ; i++)
            {
                drawGame[rowRange[j]][columnRange[i]] ="E";
            }
        }
        for (let i=0; i<columnRange.length;i++){
            columnRange[i] += 1;
        }
    } else if (event.key == 'a') {
         for(let j=0; j<columnRange.length ; j++)
          {
             drawGame[rowRange][columnRange[j]] ="E";
             columnRange[j] -= 1;
          }
       }
 }

function remove()
{
    for(let i=0; i<15; i++)
    {
        removeOneRow(i);
    }
}

function removeOneRow(row)
{
    let rows = 15;
    let count =0;

    for(let i=0; i< 15; i+=1)
    {
         if( drawGame[row][i] == "O" )
         {
            count = count+1;
         }
    }

    if (count==15)
    {
        drawGame.splice(row,1);
        score = score+1;
        drawGame.unshift(["E","E","E","E","E","E","E","E","E","E","E","E","E","E","E"])
    }
    
}

function generateScore()
{
   let scores = {x:[0,10,2,7,5], y:[0,6,9,14,3]};
   for(let i = 0; i < scores.x.length; i++){
       let row = scores.x[i];
       let column =scores.y[i];
       drawGame[row][column] = "$";
   }
}
