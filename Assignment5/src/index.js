//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

let counter = 0;
function count()
{
    //need to change each label to "correct" or "incorrect"   
    //based on if the input value is the right answer or not 
    
    //reset counter 
    counter = 0;

    //check result for first question by using my function checkResultForOne
    checkResultForOne("#M","#Q1", -600);
    checkResultForOne("#Y","#Q2", 200);
    checkResultForOne("#R","#Q3", -0.5);
    checkResultForOne("#X","#Q4", -88);
    checkResultForOne("#A","#Q5", 314);

    //need to display # of correct and # of incorrect in the end
    // get the label
    let correctLabel = document.querySelector("#total");
    // somehow get total of correct questions (0..5)
    // set label to "Total: # correct "
    correctLabel.innerHTML = "Total: " + counter + "correct";
}

function checkResultForOne(answerId, labelId, correctResult ){
    //get the input value 
    let answer = Number(document.querySelector(answerId).value);
    // console.log(answer);
    //get the label
    let checkLabel = document.querySelector(labelId);
    // console.log(checkLabel.innerHTML);
    //know the correct result (because it is parameter )
    //compare them (expression)
    //if true, then change label to "correct"
    //otherwise, change label to "incorrect"
    if (answer === correctResult) {
        checkLabel.innerHTML = "correct";
        counter++;
    }
    else {
        checkLabel.innerHTML = "incorrect";
    }
}

function inRange(minN,number,maxN)
{ 
 
    if(minN<=number  && number<=maxN) 
      {  return true;
      }
     else
     {
      return false;
     }
  

}
