/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

let counter = 0;
function count()
{
    //need to change each label to "correct" or "incorrect"   
    //based on if the input value is the right answer or not 
    
    //reset counter 
    counter = 0;

    //check result for first question by using my function checkResultForOne
    checkResultForOne("#M","#Q1", -600);
    checkResultForOne("#Y","#Q2", 200);
    checkResultForOne("#R","#Q3", -0.5);
    checkResultForOne("#X","#Q4", -88);
    checkResultForOne("#A","#Q5", 314);

    //need to display # of correct and # of incorrect in the end
    // get the label
    let correctLabel = document.querySelector("#total");
    // somehow get total of correct questions (0..5)
    // set label to "Total: # correct "
    correctLabel.innerHTML = "Total: " + counter + "correct";
}

function checkResultForOne(answerId, labelId, correctResult ){
    //get the input value 
    let answer = Number(document.querySelector(answerId).value);
    // console.log(answer);
    //get the label
    let checkLabel = document.querySelector(labelId);
    // console.log(checkLabel.innerHTML);
    //know the correct result (because it is parameter )
    //compare them (expression)
    //if true, then change label to "correct"
    //otherwise, change label to "incorrect"
    if (answer === correctResult) {
        checkLabel.innerHTML = "correct";
        counter++;
    }
    else {
        checkLabel.innerHTML = "incorrect";
    }
}

function inRange(minN,number,maxN)
{ 
 
    if(minN<=number  && number<=maxN) 
      {  return true;
      }
     else
     {
      return false;
     }
  

}


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsQzs7QUFFQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9zcmMvaW5kZXguanNcIik7XG4iLCIvL1RoaXMgaXMgdGhlIGVudHJ5IHBvaW50IEphdmFTY3JpcHQgZmlsZS5cclxuLy9XZWJwYWNrIHdpbGwgbG9vayBhdCB0aGlzIGZpbGUgZmlyc3QsIGFuZCB0aGVuIGNoZWNrXHJcbi8vd2hhdCBmaWxlcyBhcmUgbGlua2VkIHRvIGl0LlxyXG5jb25zb2xlLmxvZyhcIkhlbGxvIHdvcmxkXCIpO1xyXG5cclxubGV0IGNvdW50ZXIgPSAwO1xyXG5mdW5jdGlvbiBjb3VudCgpXHJcbntcclxuICAgIC8vbmVlZCB0byBjaGFuZ2UgZWFjaCBsYWJlbCB0byBcImNvcnJlY3RcIiBvciBcImluY29ycmVjdFwiICAgXHJcbiAgICAvL2Jhc2VkIG9uIGlmIHRoZSBpbnB1dCB2YWx1ZSBpcyB0aGUgcmlnaHQgYW5zd2VyIG9yIG5vdCBcclxuICAgIFxyXG4gICAgLy9yZXNldCBjb3VudGVyIFxyXG4gICAgY291bnRlciA9IDA7XHJcblxyXG4gICAgLy9jaGVjayByZXN1bHQgZm9yIGZpcnN0IHF1ZXN0aW9uIGJ5IHVzaW5nIG15IGZ1bmN0aW9uIGNoZWNrUmVzdWx0Rm9yT25lXHJcbiAgICBjaGVja1Jlc3VsdEZvck9uZShcIiNNXCIsXCIjUTFcIiwgLTYwMCk7XHJcbiAgICBjaGVja1Jlc3VsdEZvck9uZShcIiNZXCIsXCIjUTJcIiwgMjAwKTtcclxuICAgIGNoZWNrUmVzdWx0Rm9yT25lKFwiI1JcIixcIiNRM1wiLCAtMC41KTtcclxuICAgIGNoZWNrUmVzdWx0Rm9yT25lKFwiI1hcIixcIiNRNFwiLCAtODgpO1xyXG4gICAgY2hlY2tSZXN1bHRGb3JPbmUoXCIjQVwiLFwiI1E1XCIsIDMxNCk7XHJcblxyXG4gICAgLy9uZWVkIHRvIGRpc3BsYXkgIyBvZiBjb3JyZWN0IGFuZCAjIG9mIGluY29ycmVjdCBpbiB0aGUgZW5kXHJcbiAgICAvLyBnZXQgdGhlIGxhYmVsXHJcbiAgICBsZXQgY29ycmVjdExhYmVsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiN0b3RhbFwiKTtcclxuICAgIC8vIHNvbWVob3cgZ2V0IHRvdGFsIG9mIGNvcnJlY3QgcXVlc3Rpb25zICgwLi41KVxyXG4gICAgLy8gc2V0IGxhYmVsIHRvIFwiVG90YWw6ICMgY29ycmVjdCBcIlxyXG4gICAgY29ycmVjdExhYmVsLmlubmVySFRNTCA9IFwiVG90YWw6IFwiICsgY291bnRlciArIFwiY29ycmVjdFwiO1xyXG59XHJcblxyXG5mdW5jdGlvbiBjaGVja1Jlc3VsdEZvck9uZShhbnN3ZXJJZCwgbGFiZWxJZCwgY29ycmVjdFJlc3VsdCApe1xyXG4gICAgLy9nZXQgdGhlIGlucHV0IHZhbHVlIFxyXG4gICAgbGV0IGFuc3dlciA9IE51bWJlcihkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGFuc3dlcklkKS52YWx1ZSk7XHJcbiAgICAvLyBjb25zb2xlLmxvZyhhbnN3ZXIpO1xyXG4gICAgLy9nZXQgdGhlIGxhYmVsXHJcbiAgICBsZXQgY2hlY2tMYWJlbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IobGFiZWxJZCk7XHJcbiAgICAvLyBjb25zb2xlLmxvZyhjaGVja0xhYmVsLmlubmVySFRNTCk7XHJcbiAgICAvL2tub3cgdGhlIGNvcnJlY3QgcmVzdWx0IChiZWNhdXNlIGl0IGlzIHBhcmFtZXRlciApXHJcbiAgICAvL2NvbXBhcmUgdGhlbSAoZXhwcmVzc2lvbilcclxuICAgIC8vaWYgdHJ1ZSwgdGhlbiBjaGFuZ2UgbGFiZWwgdG8gXCJjb3JyZWN0XCJcclxuICAgIC8vb3RoZXJ3aXNlLCBjaGFuZ2UgbGFiZWwgdG8gXCJpbmNvcnJlY3RcIlxyXG4gICAgaWYgKGFuc3dlciA9PT0gY29ycmVjdFJlc3VsdCkge1xyXG4gICAgICAgIGNoZWNrTGFiZWwuaW5uZXJIVE1MID0gXCJjb3JyZWN0XCI7XHJcbiAgICAgICAgY291bnRlcisrO1xyXG4gICAgfVxyXG4gICAgZWxzZSB7XHJcbiAgICAgICAgY2hlY2tMYWJlbC5pbm5lckhUTUwgPSBcImluY29ycmVjdFwiO1xyXG4gICAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiBpblJhbmdlKG1pbk4sbnVtYmVyLG1heE4pXHJcbnsgXHJcbiBcclxuICAgIGlmKG1pbk48PW51bWJlciAgJiYgbnVtYmVyPD1tYXhOKSBcclxuICAgICAgeyAgcmV0dXJuIHRydWU7XHJcbiAgICAgIH1cclxuICAgICBlbHNlXHJcbiAgICAge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgfVxyXG4gIFxyXG5cclxufVxyXG4iXSwic291cmNlUm9vdCI6IiJ9