//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

function create()
{
  let numberOfPoints=document.querySelector("#RangeA");
  let innerRadius=document.querySelector("#ir");
  let outerRadius=document.querySelector("#or");
  numberOfPoints = Number(numberOfPoints.value);
  innerRadius = Number(innerRadius.value);
  outerRadius= Number(outerRadius.value);

  let points = generatePoints(numberOfPoints,innerRadius,outerRadius);
  let pointsString = generateSvgPolygon(points);
  let output = document.querySelector("#poly");
  output.setAttribute("points",pointsString)
 }

 function generatePoints(n,ir,or)
 {
   let points = [];
  let angle = 0;
  let increment = 360/ (2 *n);
  let xVal=0;
  let yVal=0;
  for(let i=0; i<n*2; i++)
  {
    if ( i %2 == 0)
    {
      xVal = or*Math.cos(angle*Math.PI/180);
      yVal = or*Math.sin(angle*Math.PI/180);
    }
    else
    {
      xVal=ir*Math.cos(angle*Math.PI/180);
      yVal =ir*Math.sin(angle*Math.PI/180);    
    }
     var p = {x: xVal,y: yVal}
     points.push(p);
     console.log(angle);
     console.log(p);
     angle = angle + increment; 
  }

    return points;
 }

 function generateSvgPolygon(points)
 {
   // use for loop
   // create string with points
   // return the string

   let pointsString = "";
   for(let j=0; j<points.length; j++)
     {
        pointsString += points[j].x + " " + points[j].y + " " ;    
      }
      return pointsString;
 }

 window.onload = create;


